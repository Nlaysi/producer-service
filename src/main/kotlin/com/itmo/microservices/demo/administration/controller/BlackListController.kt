package com.itmo.microservices.demo.administration.controller

import com.itmo.microservices.demo.utils.BlackListRequest
import com.itmo.microservices.demo.utils.BlackListService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/blacklist")
@Transactional
class BlackListController(val blackListService: BlackListService) {

    @PostMapping("/add")
    @Operation(
        summary = "Add wiki page to black list",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "ERROR", responseCode = "403", content = [Content()])
        ],
        security = [SecurityRequirement(name = "bearerAuth")]
    )
    fun add(@RequestBody req: BlackListRequest): ResponseEntity<String> {
        val data = SecurityContextHolder.getContext().authentication.principal as UserDetails

        if (data.username != "admin")
            return ResponseEntity(HttpStatus.valueOf(403))

        blackListService.addToBlackList(req.wikiName)

        return ResponseEntity("OK", HttpStatus.OK)
    }

    @PostMapping("/remove")
    @Operation(
        summary = "Remove wiki page from black list",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "ERROR", responseCode = "403", content = [Content()])
        ],
        security = [SecurityRequirement(name = "bearerAuth")]
    )
    fun remove(@RequestBody req: BlackListRequest): ResponseEntity<String> {
        val data = SecurityContextHolder.getContext().authentication.principal as UserDetails

        if (data.username != "admin")
            return ResponseEntity(HttpStatus.valueOf(403))

        blackListService.removeFromBlackList(req.wikiName)

        return ResponseEntity("OK", HttpStatus.OK)
    }

}