package com.itmo.microservices.demo.users.impl.entity

import com.itmo.microservices.demo.users.api.model.Status
import org.hibernate.annotations.GenericGenerator
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "tokens")
data class User(
    @Id
    var id: String,
    var name: String,
    var sub: Subscribe,
    var status: Status,
) {
    constructor() : this(UUID.randomUUID().toString(), "", Subscribe.FREE, Status.OFFLINE)

    constructor(
        name: String,
        s: Subscribe,
        status: Status,
    ) : this(UUID.randomUUID().toString(), name, s, status)

    enum class Subscribe {
        FREE,
        STANDARD,
        UNLIMITED
    }
}