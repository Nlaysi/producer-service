package com.itmo.microservices.demo.users.api.model

import com.itmo.microservices.demo.users.impl.entity.User

data class UserRequestDto(
    val name: String,
    val subscribe: User.Subscribe
)