package com.itmo.microservices.demo.utils.entity

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "black_list")
class BlackList() {
    @Id
    @Column(name = "id", nullable = false)
    var id: Int = 0

    val createdDate: Date = Date()

    constructor(id: Int) : this()
}