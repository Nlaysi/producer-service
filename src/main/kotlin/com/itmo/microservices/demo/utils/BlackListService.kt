package com.itmo.microservices.demo.utils

import com.itmo.microservices.demo.utils.entity.BlackList
import com.itmo.microservices.demo.utils.repos.BlackListRepository
import org.springframework.stereotype.Service

@Service
class BlackListService (val blackListRepository: BlackListRepository) {

    fun checkBlackList(wiki: String) : Boolean {
        if (blackListRepository.existsById(wiki.hashCode())) {
            return false
        }
        return true
    }

    fun addToBlackList(wiki: String) {
        if (!blackListRepository.existsById(wiki.hashCode())) {
            val black = BlackList()
            black.id = wiki.hashCode()
            blackListRepository.save(black)
        }
    }

    fun removeFromBlackList(wiki: String) {
        if (blackListRepository.existsById(wiki.hashCode())) {
            blackListRepository.deleteById(wiki.hashCode())
        }
    }
}