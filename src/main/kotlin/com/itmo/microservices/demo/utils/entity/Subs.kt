package com.itmo.microservices.demo.utils.entity

import com.itmo.microservices.demo.users.api.model.Status
import com.itmo.microservices.demo.users.impl.entity.User
import java.time.LocalDate
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "subs")
class Subs(id: String) {
    constructor() : this("") {

    }

    @Id
    @Column(name = "id", nullable = false)
    val id: String? = id

    var date: LocalDate = LocalDate.now()

    var count = 1
}