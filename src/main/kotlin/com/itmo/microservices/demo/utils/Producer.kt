package com.itmo.microservices.demo.utils

import com.itmo.microservices.commonlib.annotations.InjectEventLogger
import com.itmo.microservices.commonlib.logging.EventLogger
import com.itmo.microservices.commonlib.logging.NotableEvent
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Service

interface KafkaProducerService {
    fun send(message: String?)
}

@Service
class Producer : KafkaProducerService {
    @Autowired
    private val kafkaTemplate: KafkaTemplate<String, String>? = null

    @InjectEventLogger
    private lateinit var logger: EventLogger

    @Value("\${app.kafka.producer.topic}")
    private val topic: String? = null

    override fun send(message: String?) {
        logger.info(ProducerNotableEvents.MESSAGE_SENT, message)
        kafkaTemplate!!.send(topic!!, message)
    }

}

enum class ProducerNotableEvents(private val template: String): NotableEvent {
    MESSAGE_SENT("message sent: {}");

    override fun getTemplate(): String {
        return template
    }

    override fun getName(): String {
        return name
    }
}