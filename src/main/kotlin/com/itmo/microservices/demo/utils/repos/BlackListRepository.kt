package com.itmo.microservices.demo.utils.repos

import com.itmo.microservices.demo.utils.entity.BlackList
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface BlackListRepository : JpaRepository<BlackList, Int> {
}