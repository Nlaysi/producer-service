package com.itmo.microservices.demo.utils

import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Service

interface KafkaConsumerService {
    fun receive(message: String?)
}

class Consumer {
    @KafkaListener(topics = ["#{'\${app.kafka.consumer.topic}'.split(',')}"])
    fun receive(@Payload message: String?) {
        println(message)
    }
}