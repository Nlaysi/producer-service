package com.itmo.microservices.demo.utils.repos

import com.itmo.microservices.demo.utils.entity.Subs
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface SubsRepository : JpaRepository<Subs, String> {

}