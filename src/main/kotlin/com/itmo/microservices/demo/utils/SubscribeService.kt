package com.itmo.microservices.demo.utils

import com.itmo.microservices.demo.users.impl.entity.User
import com.itmo.microservices.demo.utils.entity.Subs
import com.itmo.microservices.demo.utils.repos.SubsRepository
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.util.*

@Service
class SubscribeService(val subsRepository: SubsRepository) {

    fun checkSubscribe(name: String, subscribe: User.Subscribe) : Boolean {

        if (!subsRepository.existsById(name)) {
            val sub = Subs(name)
            subsRepository.save(sub)
            return true
        }
        else {
            val sub = subsRepository.getById(name)
            if (sub.date != LocalDate.now()) {
                sub.count = 1
                sub.date = LocalDate.now()
                subsRepository.save(sub)
                return true
            }
            if (subscribe == User.Subscribe.STANDARD && sub.count < 20) {
                sub.count++
                subsRepository.save(sub)

                return true
            }
            if (subscribe == User.Subscribe.FREE && sub.count < 1) {
                sub.count++
                subsRepository.save(sub)

                return true
            }
            if (subscribe == User.Subscribe.UNLIMITED) {
                return true
            }

            return false
        }

    }

}