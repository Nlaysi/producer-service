package com.itmo.microservices.demo.utils

data class BlackListRequest (val wikiName: String)
