package com.itmo.microservices.demo.consumer.service

import com.itmo.microservices.demo.consumer.service.repository.WikiRepository
import com.itmo.microservices.demo.consumer.service.entity.WikiInfo
import org.springframework.stereotype.Service
import java.util.*

@Service
class ConsumerService(val wikiRepository: WikiRepository) {

    fun get(id: UUID): WikiInfo? {
        if (wikiRepository.existsById(id)) {
            return wikiRepository.getById(id)
        }
        return null
    }

}