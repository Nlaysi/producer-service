package com.itmo.microservices.demo.consumer.service.entity

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "wiki_info")
class WikiInfo (
    @Id
    @Column(name = "id", nullable = false)
    var id: UUID? = null

) {
    var length : Int = 0

    constructor() : this(UUID.randomUUID()) {

    }
}