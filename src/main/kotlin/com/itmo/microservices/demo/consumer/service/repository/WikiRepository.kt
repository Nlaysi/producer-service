package com.itmo.microservices.demo.consumer.service.repository

import com.itmo.microservices.demo.consumer.service.entity.WikiInfo
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface WikiRepository  : JpaRepository<WikiInfo, UUID> {
}