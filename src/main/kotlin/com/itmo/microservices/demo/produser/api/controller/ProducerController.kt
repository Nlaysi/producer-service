package com.itmo.microservices.demo.produser.api.controller

import com.itmo.microservices.demo.consumer.service.ConsumerService
import com.itmo.microservices.demo.produser.api.service.ProducerResponseDto
import com.itmo.microservices.demo.produser.api.service.ProducerService
import com.itmo.microservices.demo.produser.SendWikiDto
import com.itmo.microservices.demo.consumer.service.entity.WikiInfo
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import org.springframework.http.ResponseEntity
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/wiki")
@Transactional
class ProducerController(val service: ProducerService, val consumerService: ConsumerService) {

    @PostMapping
    @Operation(
        summary = "Send to kafka",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "ERROR", responseCode = "403", content = [Content()])
        ],
        security = [SecurityRequirement(name = "bearerAuth")]
    )
    fun send(req: SendWikiDto): ResponseEntity<ProducerResponseDto> {
        val data = SecurityContextHolder.getContext().authentication.principal as UserDetails
        return service.send(data.username, req)
    }

    @GetMapping
    @Operation(
        summary = "Get answer",
        responses = [
            ApiResponse(description = "OK", responseCode = "200"),
            ApiResponse(description = "ERROR", responseCode = "403", content = [Content()])
        ],
        security = [SecurityRequirement(name = "bearerAuth")]
    )
    fun get(@RequestParam id: UUID): WikiInfo? {
        return consumerService.get(id)
    }
}