package com.itmo.microservices.demo.produser.api.service

import com.itmo.microservices.demo.produser.SendWikiDto
import com.itmo.microservices.demo.users.impl.repository.UserRepository
import com.itmo.microservices.demo.utils.BlackListService
import com.itmo.microservices.demo.utils.Consumer
import com.itmo.microservices.demo.utils.Producer
import com.itmo.microservices.demo.utils.SubscribeService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import java.util.*

@Service
class ProducerService(
    private val userRepository: UserRepository,
    private val subscribeService: SubscribeService,
    private val blackListService: BlackListService,
    private val producer: Producer
    ) {

    @Async
    fun send(username: String, req: SendWikiDto) : ResponseEntity<ProducerResponseDto> {
        val user = userRepository.findUserByName(username)
        if (
            subscribeService.checkSubscribe(user!!.name, user.sub) &&
            blackListService.checkBlackList(req.wikiPageFrom) &&
            blackListService.checkBlackList(req.wikiPageTo)
        ) {
            val id = UUID.randomUUID()
            val dto = ProducerResponseDto()
            dto.requestID = id

            val message = "$id:${req.wikiPageFrom}:${req.wikiPageTo}"
            producer.send(message)

            return ResponseEntity(dto, HttpStatus.OK)
        }
        else {
            return ResponseEntity(ProducerResponseDto(), HttpStatus.BAD_REQUEST)
        }
    }
}