package com.itmo.microservices.demo.produser

data class SendWikiDto(val wikiPageFrom: String, val wikiPageTo: String)

